const moviesDataset = require('./movie_db.json')
const { movies, genres } = moviesDataset

const filterAndSort = (props) => {
    const { array, f_property, f_string, s_property, s_type } = props

    const filteredMovies = array.filter((m) => m[f_property].includes(f_string));
    filteredMovies.sort((a, b) => {
        let sorted = (s_type === "ASC") ?
            a[s_property] - b[s_property] :
            b[s_property] - a[s_property]

        return sorted
    });

    return filteredMovies
}

const groupAndCount = (props) => {
    const { array, property } = props

    const groupedArray = array.reduce((grouped, obj) => {
        const key = obj[property];
        if (!grouped[key]) {
            grouped[key] = 0;
        }
        grouped[key] += 1;
        
        return grouped;
    }, {});

    return groupedArray
}

//"props" to change arrcording to criteria
const props1 = {
    array: movies,
    f_property: "actors",
    f_string: "Leonardo DiCaprio",
    s_property: "runtime",
    s_type: "DESC"
}

const props2 = {
    array: movies,
    property: "year"
}

const result1 = filterAndSort(props1)
const result2 = groupAndCount(props2)

//RESULTS
console.log("----- Sort and Filter Function -----")
console.log(result1)
console.log("----- Group and Count Function -----")
console.log(result2)
